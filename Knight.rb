require 'pp'
require_relative 'ArrayX'
# > set RUBY_THREAD_VM_STACK_SIZE
# require_relative 'no_sltd'
require_relative 'TCO'

class Array
	def sigma
		self.inject(0){|s,e| s+=e}
	end

	def volume
		self.inject(1){|s,e| s*=e}
	end
end


class KnightTour
	attr_accessor :board, :xidx, :count, :timelimit, :totaltime

	def initialize(dim)
		@dim = dim
		@board = ArrayX.new(dim)
		@moves = all_moves(dim.size)
		@start_time = Time.now
		@timelimit = 30
		@totaltime = 0
	end


	def search(idx, visited)
		catch :esc do

			@board.set(idx, visited)

			laptime = Time.now - @start_time
			timeout =  laptime > @timelimit
			finished = visited >= @dim.volume - 1

			if timeout || finished
				@xidx = idx
				@count = visited + 1
				@totaltime += laptime
				throw :esc
			end
    
			empty_neighbours = []
			@moves.each{|e|
				nb = idx.zip(e).map{|a,b| a+b}
				empty_neighbours << nb if in_range_and_empty(nb)
			}

			# Warnsdorff's rule
			cands = empty_neighbours.sort_by{|v|
				@moves.map{|e|
					mv = v.zip(e).map{|a,b| a+b}
					in_range_and_empty(mv) ? 1 : 0
				}.sigma
			}

#			cands.each{|e| 
#				search(e, visited+1) 
#			}
#			@board.set(idx, nil)

			search(cands[0], visited+1)
		end
	end
	TCO :search


	def in_range_and_empty(mv)
		mv.inject(true){|s,e| e>=0 && s} && 
		mv.zip(@dim).inject(true){|s,e| e[0]<e[1] && s} && 
		@board.get(mv)==nil
	end


	def all_moves(dimension)
		move = [0]*(dimension-2) + [-2, -1, 1, 2]
		move = move.permutation(dimension).to_a
		move.select{|e|
			a = e.map{|x| x**2}
			a = a.inject(0){|s,e| s+=e}
			a = a**0.5
			a == 5**0.5
		}
	end
end