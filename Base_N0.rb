﻿require 'bigdecimal'
require 'bigdecimal/math'

$base_n = [nil]*2
notation = [nil]*2

(2..36).each{|base|
	precision = 1000
	num = BigMath::sqrt(BigDecimal.new('2'), precision*2)
	int = num.to_i
	frac = num-int

	char = (0..9).to_a + ('A'..'Z').to_a

	result_i = []
	result_f = []

	if base > 36
		puts 'error (base should be less than 36)'
		exit
	end

	while int > 0
		result_i << int % base
	    int = int/base
	end

	result_i.reverse!
	notation_i = result_i.map{|e| char[e]}
	notation_i = notation_i.join('')

	while frac > 0 && result_f.size<precision
		ii = (frac*base).to_i
		ff = frac*base-ii

		result_f << ii
		frac = ff
	end

	notation_f = result_f.map{|e| char[e]}
	notation_f = notation_f.join('')
	notation << notation_i+"."+notation_f

	$base_n << result_i+result_f
}

=begin
f = File.open("base-n.txt",'w')
f.puts notation
f.close
=end