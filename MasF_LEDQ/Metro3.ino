#define SpinA 2
#define SpinB 3
#define SpinC 4
#define Ipin 6
#define RpinA 10
#define GpinA 11
#define RpinB 12
#define GpinB 13

int pattern0[1] = {0};  // blank
int pattern1[1] = {1};  // stop
int pattern2[2] = {2, 0}; // double
int pattern3[1] = {3};  // one-shot
int pattern4[4] = {1, 2, 2, 2}; // 4 counts
int pattern5[1] = {2};
int pattern6[4] = {2, 2, 2, 0};
int pattern7[4] = {3, 3, 3, 0};

int pattern11[2] = {1, 2}; // 2 counts
int pattern12[3] = {1, 2, 2}; // 3 counts
int pattern13[4] = {1, 2, 2, 2}; // 4 counts
int pattern14[4] = {1, 3, 3, 3}; // 4 counts ready
int pattern15[5] = {1, 2, 2, 2, 2}; // 5 counts
int pattern16[5] = {1, 3, 3, 3, 3}; // 5 counts ready
int pattern17[7] = {1, 2, 2, 2, 2, 2, 2}; // 7 counts
int pattern18[7] = {1, 3, 3, 3, 3, 3, 3}; // 7 counts ready
int pattern19[10] = {1, 0, 2, 0, 2, 0, 2, 0, 2, 0}; // 5 counts double
int pattern20[10] = {1, 0, 3, 0, 3, 0, 3, 0, 3, 0}; // 5 counts double ready

int pattern23[8] = {1, 0, 3, 0, 3, 0, 3, 0}; // ready
int pattern24[4] = {1, 3, 0, 0}; // mark
int pattern25[6] = {2, 0, 2, 0, 2, 0}; // after mark
int pattern26[8] = {1, 0, 2, 0, 2, 0, 2, 0}; // count

int testptrnA[12] = {1, 0, 1, 0, 2, 2, 2, 2, 3, 0, 3, 0};
int testptrnB[12] = {1, 1, 1, 1, 2, 0, 2, 0, 0, 3, 0, 3};

// ratio of lighting duration (1/n)
int pat_ratio[] = {
  1, 1, 8, 1, 8,
  1, 1, 1, 1, 2,
  2, 4, 4, 4, 4,
  4, 4, 4, 4, 4,
  4, 1, 1, 2, 2,
  2, 2, 2
};

template<typename T, int size>
int Arysize(T(&)[size]) {
  return size;
}

boolean debug = true;

class Metro {
    int ledpinR, ledpinG, seq_size;
    int* seq;

    int light, beat, count, seqn, loc;
    int patn, tempo, repeat;
    float beat_dur, light_dur, time_next;
    unsigned long time_now, time_start;

  public:
    Metro(int r, int g) {
      ledpinR = r;
      ledpinG = g;

      pinMode(ledpinR, OUTPUT);
      pinMode(ledpinG, OUTPUT);
    }

    void Next() {
      patn = pgm_read_word_near(&seq[seqn]);
      tempo = pgm_read_word_near(&seq[seqn + 1]);
      repeat = pgm_read_word_near(&seq[seqn + 2]);
      beat_dur = 60.0 / tempo * 1000;
      light_dur = beat_dur / pat_ratio[patn];
    }

    void Change(int* s, int q, int n) {
      seq = s;
      seq_size = q;
      loc = n * 3;
      light = LOW;
    }

    void Off() {
      light = LOW;
      digitalWrite(ledpinR, light);
      digitalWrite(ledpinG, light);

      if (debug == false) {
        Serial.print(ledpinR - RpinA + 4);
        Serial.print(ledpinG - RpinA + 4);
      }
    }

    void Start(unsigned long t_now) {
      seqn = loc;
      beat = 0;
      count = 0;
      Next();
      time_start = t_now;
      time_next = 0;
    }

    void Update() {
      time_now = millis() - time_start;

      if ((light == HIGH) && (time_now >= time_next))
      {
        Off();
        time_next += beat_dur - light_dur;

        if (count == repeat) {
          seqn += 3;
          if (seqn < seq_size) {
            beat = 0;
            count = 0;
            Next();
          }
        }
      }

      else if ((light == LOW) && (time_now >= time_next))
      {
        int state, len;
        switch (patn) {
          case 0:
            state = pattern0[beat];
            len = Arysize(pattern0);
            break;
          case 1:
            state = pattern1[beat];
            len = Arysize(pattern1);
            break;
          case 2:
            state = pattern2[beat];
            len = Arysize(pattern2);
            break;
          case 3:
            state = pattern3[beat];
            len = Arysize(pattern3);
            break;
          case 4:
            state = pattern4[beat];
            len = Arysize(pattern4);
            break;
          case 6:
            state = pattern1[beat];
            len = Arysize(pattern1);
            break;
          case 7:
            state = pattern5[beat];
            len = Arysize(pattern5);
            break;
          case 8:
            state = pattern3[beat];
            len = Arysize(pattern3);
            break;
          case 9:
            state = pattern6[beat];
            len = Arysize(pattern6);
            break;
          case 10:
            state = pattern7[beat];
            len = Arysize(pattern7);
            break;
          case 11:
            state = pattern11[beat];
            len = Arysize(pattern11);
            break;
          case 12:
            state = pattern12[beat];
            len = Arysize(pattern12);
            break;
          case 13:
            state = pattern13[beat];
            len = Arysize(pattern13);
            break;
          case 14:
            state = pattern14[beat];
            len = Arysize(pattern14);
            break;
          case 15:
            state = pattern15[beat];
            len = Arysize(pattern15);
            break;
          case 16:
            state = pattern16[beat];
            len = Arysize(pattern16);
            break;
          case 17:
            state = pattern17[beat];
            len = Arysize(pattern17);
            break;
          case 18:
            state = pattern18[beat];
            len = Arysize(pattern18);
            break;
          case 19:
            state = pattern19[beat];
            len = Arysize(pattern19);
            break;
          case 20:
            state = pattern20[beat];
            len = Arysize(pattern20);
            break;
          case 21:
            state = testptrnA[beat];
            len = Arysize(testptrnA);
            break;
          case 22:
            state = testptrnB[beat];
            len = Arysize(testptrnB);
            break;
          case 23:
            state = pattern23[beat];
            len = Arysize(pattern23);
            break;
          case 24:
            state = pattern24[beat];
            len = Arysize(pattern24);
            break;
          case 25:
            state = pattern25[beat];
            len = Arysize(pattern25);
            break;
          case 26:
            state = pattern26[beat];
            len = Arysize(pattern26);
            break;
          case 27:
            state = pattern3[beat];
            len = Arysize(pattern3);
            break;
        }

        light = HIGH;

        if (state % 2 == 1) {
          if (beat == 0) {
            Serial.println(time_now);
          }
          digitalWrite(ledpinR, light);
          if (debug == false) {
            Serial.print(ledpinR - RpinA);
          }
        }

        if (state / 2 > 0) {
          digitalWrite(ledpinG, light);
          if (debug == false) {
            Serial.print(ledpinG - RpinA);
          }
        }

        if (debug == true) {
          //          Serial.print(beat);
          //          Serial.print(" ");
          //          Serial.println(count);
        }

        time_next += light_dur;
        beat = (beat + 1) % len;
        if (beat == 0) {
          count++;
        }
      }
    }
};


/* Metro seq */

// Test pattern
const unsigned int testA[] PROGMEM = {
  21, 240, 1,
};

const unsigned int testB[] PROGMEM = {
  22, 240, 1,
};

// M1
const unsigned int blank[] PROGMEM = {
  0, 1, 1,    // endless loop
};

const unsigned int ms1[] PROGMEM = {
  4, 54, 37, // How beautiful is
  3, 54, 2, // to-night.
  0, 54, 3,
  0, 60, 1,
  3, 60, 2, // How pale the Princess is
  0, 60, 20,
  3, 60, 2, // The Princess has hidden her face
  0, 60, 7,
  3, 60, 2, // Her little white hands
  0, 60, 19,
  3, 60, 2, // (awkwardly)
  0, 60, 19,
  3, 60, 2, // Ah, she is coming
  0, 60, 16,
  3, 60, 2, // Never have I seen
  0, 60, 8,
  3, 60, 2, // She is like
  0, 120, 35,
  1, 60, 2,
  0, 1, 1,
};

// M4
const unsigned int ms4[] PROGMEM = {
  0, 120, 307,
  1, 60, 2,   // (stop)
  0, 1, 1,
};

// M5
const unsigned int ms5[] PROGMEM = {
  0, 60, 32,
  3, 60, 2,   // What an uproar
  0, 60, 56,
  3, 60, 2,   // The Tetrarch has a sombre look
  0, 120, 123,
  1, 60, 2,   // (M4 stop)
  0, 120, 237,
  3, 60, 2,   // What a strange prison
  0, 1, 1,
};

// M7&8
const unsigned int ms8a[] PROGMEM = {
  0, 75, 8,
  23, 120, 1,
  26, 120, 5,
  24, 240, 1, // A
  25, 120, 1,
  26, 120, 3,
  24, 240, 1, // B
  25, 120, 1,
  26, 120, 8,
  24, 240, 1, // C
  25, 120, 1,
  26, 120, 10,
  24, 240, 1, // D
  25, 120, 1,
  26, 120, 6,
  24, 240, 1, // E
  25, 120, 1,
  26, 120, 5,
  24, 240, 1, // F
  25, 120, 1,
  26, 120, 5,
  24, 240, 1, // G
  25, 120, 1,
  26, 120, 4,
  24, 240, 1, // H
  25, 120, 1,
  26, 120, 6,
  24, 240, 1, // I
  25, 120, 1,
  26, 120, 5,
  24, 240, 1, // J
  25, 120, 1,
  26, 120, 6,
  24, 240, 1, // K
  25, 120, 1,
  26, 120, 7,
  24, 240, 1, // L
  25, 120, 1,
  26, 120, 7,
  24, 240, 1, // M
  25, 120, 1,
  26, 120, 5,
  24, 240, 1, // N
  25, 120, 1,
  26, 120, 6,
  24, 240, 1, // O
  25, 120, 1,
  26, 120, 7,
  24, 240, 1, // P
  25, 120, 1,
  26, 120, 3,
  0, 1, 1,
};

const unsigned int ms8b[] PROGMEM = {
  0, 75, 8,
  23, 120, 1,
  26, 120, 5,
  24, 240, 1, // A
  25, 120, 1,
  26, 120, 3,
  24, 240, 1, // B
  25, 120, 1,
  26, 120, 8,
  24, 240, 1, // C
  25, 120, 1,
  26, 120, 10,
  24, 240, 1, // D
  25, 120, 1,
  26, 120, 6,
  24, 240, 1, // E
  25, 120, 1,
  26, 120, 5,
  24, 240, 1, // F
  25, 120, 1,
  26, 120, 5,
  24, 240, 1, // G
  25, 120, 1,
  26, 120, 4,
  24, 240, 1, // H
  25, 120, 1,
  26, 120, 6,
  0, 1, 1,
};

// M10
const unsigned int ms10a[] PROGMEM = {
  0, 120, 2,
  2, 53, 17,
  0, 120, 28,
  2, 53, 7,
  0, 120, 23,
  2, 75, 21,
  0, 120, 10,
  2, 77, 32,
  0, 120, 23,
  2, 64, 11,
  0, 120, 22,
  2, 66, 18,
  0, 120, 37,
  2, 66, 30,
  0, 120, 13,
  2, 67, 27,
  0, 1, 1,
};

const unsigned int ms10b[] PROGMEM = {
  0, 60, 1,
  3, 60, 2,
  0, 60, 58,
  3, 60, 2,
  0, 60, 19,
  3, 60, 2,
  0, 60, 8,
  3, 60, 2,
  0, 60, 22,
  3, 60, 2,
  0, 60, 40,
  3, 60, 2,
  0, 60, 6,
  3, 60, 2,
  0, 60, 50,
  3, 60, 2,
  0, 60, 15,
  3, 60, 2,
  0, 60, 29,
  3, 60, 2,
  0, 60, 37,
  3, 60, 2,
  0, 60, 13,
  3, 60, 2,
  0, 60, 25,
  3, 60, 2,
  0, 60, 1,
};

//M11
const unsigned int ms11a[] PROGMEM = {
  16, 135, 1, // A
  19, 135, 1,
  16, 135, 1,
  19, 135, 3,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 2,
  16, 135, 1,
  19, 135, 3,
  16, 135, 1,
  19, 135, 2,
  16, 135, 1,
  19, 135, 1,
  0, 27, 8,
  27, 162, 114, // B
  0, 54, 2,

  14, 72, 2, // C
  13, 72, 6,
  12, 54, 6,
  13, 36, 2,
  11, 36, 1,
  1, 54, 1,

  16, 135, 1, // D
  19, 135, 2,
  16, 135, 1,
  19, 135, 3,
  16, 135, 1,
  19, 135, 1,
  0, 27, 10,
  0, 135, 2,
  20, 135, 1,
  19, 135, 2,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 5,
  0, 27, 1,
};

const unsigned int ms11b[] PROGMEM = {
  16, 135, 1, // A
  19, 135, 1,
  16, 135, 1,
  19, 135, 3,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 2,
  16, 135, 1,
  19, 135, 3,
  16, 135, 1,
  19, 135, 2,
  16, 135, 1,
  19, 135, 1,
  0, 27, 8,
  27, 162, 114, // B
  0, 54, 2,

  18, 63, 1, // C
  17, 63, 3,
  15, 45, 3,
  12, 27, 2,
  12, 54, 1,
  0, 54, 1,

  16, 135, 1, // D
  19, 135, 2,
  16, 135, 1,
  19, 135, 3,
  16, 135, 1,
  19, 135, 1,
  0, 27, 10,
  0, 135, 2,
  20, 135, 1,
  19, 135, 2,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 1,
  16, 135, 1,
  19, 135, 5,
  0, 27, 1,
};

/* Main */

#define TIME_CHAT 20
#define PGMS 13
unsigned long time_now, time_prev = 0;
unsigned long time_n, time_p = 0;
volatile int sel = 0;
volatile boolean go = false;
volatile boolean tgl = false;
volatile int* ar0, ar1;
volatile int as0, as1;
byte idc = 0;
byte loc0, loc1;
Metro me0(RpinA, GpinA);
Metro me1(RpinB, GpinB);

void setup() {
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(SpinA), seq_play, CHANGE);
}

void loop() {
  indicator();

  if (go) {
    me0.Update();
    me1.Update();
  } else if (digitalRead(SpinB) == HIGH) {
    seq_select(1);
  } else if (digitalRead(SpinC) == HIGH) {
    seq_select(4);
  }
}

/* Button Control */
void seq_select(int s) {
  time_now = millis();
  if (time_now - time_prev > TIME_CHAT) {
    sel = (sel + s) % PGMS;
    switch (sel) {
      case 0:
        ar0 = testA;
        as0 = Arysize(testA);
        ar1 = testB;
        as1 = Arysize(testB);
        break;
      case 1:
        ar0 = ms1;
        as0 = Arysize(ms1);
        ar1 = blank;
        as1 = Arysize(blank);
        break;
      case 2:
        ar0 = ms5;
        as0 = Arysize(ms5);
        ar1 = ms4;
        as1 = Arysize(ms4);
        break;
      case 3:
      case 6:
      case 7:
      case 8:
      case 9:
        ar0 = ms8a;
        as0 = Arysize(ms8a);
        ar1 = ms8b;
        as1 = Arysize(ms8b);
        break;
      case 4:
        ar0 = ms10a;
        as0 = Arysize(ms10a);
        ar1 = ms10b;
        as1 = Arysize(ms10b);
        break;
      case 5:
      case 10:
      case 11:
      case 12:
        ar0 = ms11a;
        as0 = Arysize(ms11a);
        ar1 = ms11b;
        as1 = Arysize(ms11a);
        break;
    }
    int loc0[PGMS] = {0, 0, 0, 0, 0,
                      0, 3, 15, 27, 39,
                      21, 23, 29
                     };
    int loc1[PGMS] = {0, 0, 0, 0, 0,
                      0, 3, 15, 27, 27,
                      21, 23, 29
                     };
    me0.Change(ar0, as0, loc0[sel]);
    me1.Change(ar1, as1, loc1[sel]);
  }
  time_prev = time_now;
}

void seq_play() {
  time_now = millis();
  if (time_now - time_prev > TIME_CHAT) {
    if (!tgl) {
      go = !go;
      if (go) {
        me0.Start(time_now);
        me1.Start(time_now);
      } else {
        me0.Off();
        me1.Off();
      }
    }
    tgl = !tgl;
  }
  time_prev = time_now;
}

/* Indicator */
void seqnum() {
  for (int i = 0; i < 4; i++) {
    digitalWrite(i + Ipin, (int)(sel / pow(2, i)) % 2);
  }
}

void indicator() {
  if (go) {
    time_n = millis();
    if (time_n - time_p > 50) {
      for (int i = 0; i < 4; i++) {
        digitalWrite(i + Ipin, LOW);
      }
      idc = (idc + 1) % 10;
      if (idc < 4) {
        digitalWrite(idc + Ipin, HIGH);
      } else {
        seqnum();
      }
      time_p = time_n;
    }
  } else {
    seqnum();
  }
}

