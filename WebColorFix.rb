f = open("Processing/preferences_orig.txt")
g = open("Processing/preferences.txt", 'w')

dat = f.readlines
dat.each.with_index{|ln, n|
	if ln =~ /editor/
		col = ln.scan(/#[0-9a-fA-F]{6}/)
		col.each{|co|
			u = [*0..2].map{|e| co[e*2+1, 2].hex}
			v = u.map{|e|
				case u.sort.index(e)
				when 0,1
					h = [e+32, 255].min
				when 2
					h = [e+96, 255].min
				end
				"%02X" % h
			}.join
			v = "#"+v
			puts "#{co} > #{v}"
			dat[n] = ln.gsub(co, v)
		}
	end
}

dat.each{|e| g.puts e}
f.close
g.close
