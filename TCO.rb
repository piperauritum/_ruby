# tail call optimization
# http://athos.hatenablog.com/entry/20110119/p1

class Module
	def TCO(name)
		continue = []
		first = true
		arguments = nil

		private_name = "private_" + name.to_s
		alias_method private_name, name
		private private_name

		proc = lambda do |*args|
			if first
				first = false
				while true
					result = send(private_name, *args)
					if result.equal? continue
						args = arguments
					else
						first = true
						return result
					end
				end
			else
				arguments = args
				continue
			end
		end
		define_method name, proc
	end
end