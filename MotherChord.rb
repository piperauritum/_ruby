class Array
	# sum of array
	def sigma
		inject(:+)
	end
	
	# convert the intervals to array of pitch
	def itv_pch(mod=0)
		self.inject([0]){|arr,n|
			pc = arr.last+n
			pc %= mod if mod>0
			arr << pc
		}
	end
	
	# check whether the intervals do not include repetition of pitch
	def chk_itv_nonrept(mod=12)	
		q = self.itv_pch(mod)
		q.uniq == q
	end
	
	def non_repdigit?
		q = self.zip(self.rotate(1)).map{|x,y| x-y}
		!(q.include?(0))
	end
	
	def reduce_rotations(ans)
		t = true
		self.size.times{|n|
			t = false if ans.include?(self.rotate(n))
		}
		t
	end
	
	def rept_permut(x=self.size, tmp=[], ans=[])
		if x==0
			if tmp[0..-2].chk_itv_nonrept &&
			tmp.non_repdigit? && tmp.reduce_rotations(ans) &&
			tmp.sigma%12==0
				ans << tmp
			end
		else
			self.each{|e|
				if tmp.chk_itv_nonrept
					self.rept_permut x-1, tmp+[e], ans
				else
					break
				end
			}
		end
		ans
	end
end

itv = [1,2,3].rept_permut(12)

p itv