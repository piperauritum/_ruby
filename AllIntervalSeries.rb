class Array
	def fwd_diff
		self.zip(self.rotate(1)).map{|x,y| (y-x)%12}
	end
	
	def chk_uniq_itv
		if self.size < 2
			true
		else
			fwd = self.fwd_diff
			fwd.pop
			fwd.uniq == fwd
		end
	end

	def permut tmp=[], ans=[]
		if self.size==0 && tmp.chk_uniq_itv
			ans << tmp
		else
			self.size.times{|n|
				if tmp.chk_uniq_itv
					copy = self.dup
					pick = copy.delete_at(n)
					copy.permut tmp+[pick], ans	
				else
					break
				end
			}
		end
		ans
	end
end

tm = Time.now
AIS = (1..11).to_a.permut [0]
AIS.each{|e| p [e, e.fwd_diff]}
puts "#{AIS.size}"
puts "#{Time.now - tm}s"