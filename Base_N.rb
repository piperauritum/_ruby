﻿require 'bigdecimal'
require 'bigdecimal/math'

def base_n_sqrt(base, precision=1000)
	num = BigMath::sqrt(BigDecimal.new('2'), precision*2)
	int = num.to_i
	frac = num-int

	result_i = []
	result_f = []

	if base > 36
		puts 'error (base should be less than 36)'
		exit
	end

	while int > 0
		result_i << int % base
	    int = int/base
	end

	result_i.reverse!

	while frac > 0 && result_f.size<precision
		ii = (frac*base).to_i
		ff = frac*base-ii

		result_f << ii
		frac = ff
	end

	[result_i, result_f]
end


def notation sq
	ii, ff = sq
	char = (0..9).to_a + ('A'..'Z').to_a
	
	int = ii.map{|e| char[e]}
	int = int.join('')
	
	frac = ff.map{|e| char[e]}
	frac = frac.join('')
	
	int+"."+frac
end

puts notation base_n_sqrt(16)