require_relative 'Knight'

dim = [10]*5
ini = [0]*dim.size
p dim.volume

getgo = false		## clear the board
start_time = Time.now

if getgo
	k = KnightTour.new(dim)
	k.search(ini, 0)

else				## continue
	load('kkkkkkk.rb')
	bx = ArrayX.new(dim)
	bx.ary = $bd

	k = KnightTour.new(dim)
	k.board = bx
	k.timelimit = 3600*6.5
	k.totaltime = $tm
	k.search($po, $ct)
end

po = k.xidx
ct = k.count
tm = k.totaltime
bd = k.board

f = File.open("kkkkkkk.rb", 'w')
f.puts "$po = #{po}"
f.puts "$ct = #{ct}"
f.puts "$tm = #{tm}"
f.puts "$bd = #{bd.ary}"
f.close

puts "last index #{po}"
puts "visited #{ct} squares"
puts "lap time: #{Time.now - start_time} sec"
puts "total time: #{tm} sec"
puts "finished" if bd.ary.flatten.uniq.size == dim.volume