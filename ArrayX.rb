class ArrayX
	attr_accessor :ary

	def initialize(dimension)
		@ary = init(dimension)
	end

	def init(dimension)
		if dimension.size>1
			Array.new(dimension[-1]){ init(dimension[0..-2]) }
		else
			Array.new(dimension[0])
		end
	end
	
	def get(idx)
		_read(idx, @ary)
	end

	def _read(idx, arr)
		if idx.size>1
			_read(idx[0..-2], arr[idx[-1]])
		else
			arr[idx[0]]
		end
	end

	def set(idx, val)
		_write(idx, @ary, val)
	end

	def _write(idx, arr, val)
		if idx.size>1
			_write(idx[0..-2], arr[idx[-1]], val)
		else
			arr[idx[0]] = val
		end
	end
end